import { BrowserModule } from "@angular/platform-browser";
import { NgModule, LOCALE_ID } from "@angular/core";
import { registerLocaleData } from '@angular/common';
import localeIt from '@angular/common/locales/it';
import { Router, RouterModule } from '@angular/router';

import { AppComponent } from "./app.component";
import { LayoutComponent } from './layout/layout.component';
import { InitPageComponent } from './init-page/init-page.component';

import { AppRoutingModule } from "./app-routing.module";

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StudentDataComponent } from './student-data/student-data.component';
import { AboutCourseComponent } from './about-course/about-course.component';
import { ExperienceCompanyComponent } from './experience-company/experience-company.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';

registerLocaleData(localeIt);

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    InitPageComponent,
    StudentDataComponent,
    AboutCourseComponent,
    ExperienceCompanyComponent
  ],
  imports: [
    BrowserModule,
    TooltipModule.forRoot(),
    AppRoutingModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    CarouselModule.forRoot(),
  ],
  providers: [{ provide: LOCALE_ID, useValue: "it-IT" }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(router: Router) {}
}
