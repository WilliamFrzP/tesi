import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { InitPageComponent } from "./init-page/init-page.component";
import { LayoutComponent } from "./layout/layout.component";
import { StudentDataComponent } from "./student-data/student-data.component";
import { ExperienceCompanyComponent } from './experience-company/experience-company.component';
import { AboutCourseComponent } from './about-course/about-course.component';

const routes: Routes = [
  { path: "", component: InitPageComponent },
  {
    path: "layout",
    component: LayoutComponent,
    children: [
      { path: "student-data", component: StudentDataComponent },
      { path: "experience-company", component: ExperienceCompanyComponent },
      { path: "about-course", component: AboutCourseComponent },
      {path: '**', redirectTo:'/layout/student-data'}
    ]
  },
  { path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
