import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-experience-company',
  templateUrl: './experience-company.component.html',
  styleUrls: ['./experience-company.component.css']
})
export class ExperienceCompanyComponent implements OnInit {
  myInterval = 0;
  activeSlideIndex = 0;
 
  slides = [
    {image: '../../assets/img/Login-img.jpg'},
    {image: '../../assets/img/Dashboard-img.jpg'},
    {image: '../../assets/img/Consumi-img.jpg'},
    {image: '../../assets/img/Produzione-img.jpg'}
  ];
  
  constructor() { }

  ngOnInit() {
  }

}
